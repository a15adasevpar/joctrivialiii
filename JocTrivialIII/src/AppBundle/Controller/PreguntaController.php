<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pregunta;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class PreguntaController extends Controller
{

    /**
     * @Route("/crearPregunta", name="crearPregunta")
     */
    public function crearPregunta()
    {        
        $pregunta = new Pregunta();
        $pregunta->setEnunciat('AAAAAA');
        $pregunta->setResposta1('11111');
        $pregunta->setResposta2('22222');
        $pregunta->setResposta3('33333');
        $pregunta->setResposta4('44444');

        $em = $this->getDoctrine()->getManager();

        $em->persist($pregunta);

        $em->flush();

        return new Response('Nova pregunta creada. Id: '.$pregunta->getId() . ' Enunciat: ' . $pregunta->getEnunciat() . ' Resposta Correcta: ' . $pregunta->getResposta1());
        
    }
    /**
     * @Route("/mostrarPreguntes", name="mostrarPreguntes")
     */
    public function mostrarPreguntes()
    {
        $preguntes = $this->getDoctrine()
            ->getRepository('AppBundle:Pregunta')
            ->findAll();

        if (count($preguntes)==0) {
            return $this->render('default/message.html.twig', array('message' => 'No preguntes found'));
        }
        return $this->render('pregunta/pregunta.html.twig', array(
            'preguntes' => $preguntes));
        /*
        $pregunta = $this->getDoctrine()
            ->getRepository('AppBundle:Pregunta')
            ->find($pregunta);
        if (!$pregunta) {
            throw $this->createNotFoundException(
                'No Pregunta found for id ' . $preguntaId
            );
        }
        */
    }

    /**
     * @Route("/inserirPreguntaBDD/{enunciatPregunta}{resposta1Pregunta}{resposta2Pregunta}{resposta3Pregunta}{resposta4Pregunta}", name="inserirPreguntaBDD")
     */
    public function inserirPreguntaBDD($enunciatPregunta, $resposta1Pregunta, $resposta2Pregunta,$resposta3Pregunta,$resposta4Pregunta,Request $request)
    {
        $novaPregunta = new Pregunta();
        $novaPregunta->setEnunciat($enunciatPregunta);
        $novaPregunta->setResposta1($resposta1Pregunta);
        $novaPregunta->setResposta2($resposta2Pregunta);
        $novaPregunta->setResposta3($resposta3Pregunta);
        $novaPregunta->setResposta4($resposta4Pregunta);

        $em = $this->getDoctrine()->getManager();

        $em->persist($novaPregunta);

        $em->flush();

        return new Response('Nova pregunta creada. Id: '.$novaPregunta->getId() . ' Enunciat: ' . $novaPregunta->getEnunciat() . ' Resposta correcta: ' . $novaPregunta->getResposta1() . ' Resposta 2: ' . $novaPregunta->getResposta2() . ' Resposta 3: ' . $novaPregunta->getResposta3() . ' Respota 4: ' . $novaPregunta->getResposta4());
    }

    /**
     * @Route("/mostrarPregunta", name="mostrarPregunta")
     */
    /*public function mostrarPregunta(Request $request) {
        //Per definir!
    }*/
    
}
