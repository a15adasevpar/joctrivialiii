<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Jugador;
use AppBundle\Entity\Pregunta;
use AppBundle\Entity\Partida;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class DefaultController extends Controller
{
    /**
     * @Route("/registreAntic", name="registreAntic")
     */
    public function registreAntic(Request $request)
    {
        $jugador = new Jugador();

        $form = $this->createFormBuilder($jugador)
            ->add('nom', TextType::class)
            ->add('posicio', IntegerType::class)
            ->add('puntuacio', IntegerType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Post'))
            ->getForm();
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $jugador = $form->getData();
            return $this->redirectToRoute('inserirJugadorBDD', array(
                'nomJugador'=> $jugador->getNom(), 
                'posicioJugador' => $jugador->getPosicio(), 
                'puntuacioJugador' => $jugador->getPuntuacio()));
        }
        return $this->render('default/new.html.twig', array('form' => $form->createView(),
        ));
    }

    /**
     * @Route("/insertarPregunta", name="insertarPregunta")
     */
    public function insertarPregunta(Request $request)
    {
        $pregunta = new Pregunta();

        $form = $this->createFormBuilder($pregunta)
            ->add('enunciat', TextType::class)
            ->add('resposta1', TextType::class)
            ->add('resposta2', TextType::class)
            ->add('resposta3', TextType::class)
            ->add('resposta4', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Post'))
            ->getForm();
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $pregunta = $form->getData();
            return $this->redirectToRoute('inserirPreguntaBDD', array(
                'enunciatPregunta'=> $pregunta->getEnunciat(), 
                'resposta1Pregunta' => $pregunta->getResposta1(),
                'resposta2Pregunta' => $pregunta->getResposta2(),
                'resposta3Pregunta' => $pregunta->getResposta3(),
                'resposta4Pregunta' => $pregunta->getResposta4()));
        }

        return $this->render('default/new.html.twig', array('form' => $form->createView(),
        ));
    }

    /**
     * @Route("/yrytyr", name="portadaAntiga")
     */
    public function mostrarPortadaAntiga(Request $request)
    {
       
       $jugadors = $this->getDoctrine()
            ->getRepository('AppBundle:Jugador')
            ->findAll();
        
        $preguntes = $this->getDoctrine()
            ->getRepository('AppBundle:Pregunta')
            ->findAll();

        // CODI DUPLICAT DE registre(Request $request)
        // -----------------------------

        $jugador = new Jugador();

        $form = $this->createFormBuilder($jugador)
            ->add('nom', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Accés'))
            ->getForm();
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $jugador = $form->getData();
            return $this->redirectToRoute('buscarJugadorBDD', array(
                'nomJugador'=> $jugador->getNom()));
        }

        // .............................
        
        //Si no hi ha preguntes -> mostrar només els jugadors
        if (count($preguntes)==0 && count($jugadors)!= 0) {
            return $this->render('portada/portada.html.twig', array('jugadors' => $jugadors));
        }
        //Si no hi ha jugadors -> mostrar només les preguntes
        else if (count($jugadors) == 0 && count($preguntes)!=0) {
            return $this->render('portada/portada.html.twig', array('preguntes' => $preguntes));
        }
        //Si no hi ha ni jugadors ni preguntes -> passar arrays buits
        elseif (count($preguntes)==0 && count($jugadors)== 0) {
            return $this->render('portada/portada.html.twig', array('preguntes','preguntes'));
        }
        //Cas normal: hi han jugadors i preguntes i es mostren els dos
        return $this->render('portada/portada.html.twig', array('jugadors' => $jugadors, 'preguntes' => $preguntes, 'form' => $form->createView()));
    }

    /**
     * @Route("/v1", name="homepage1")
     */
    /*
    public function registre_v1(Request $request)
    {
        $jugador = new Jugador();

        $form = $this->createFormBuilder($jugador)
            ->add('nom', TextType::class)
            ->add('posicio', IntegerType::class)
            ->add('puntuacio', IntegerType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Post'))
            ->getForm();
        return $this->render('default/new.html.twig', array('form' => $form->createView(),
        ));
    }*/
}
