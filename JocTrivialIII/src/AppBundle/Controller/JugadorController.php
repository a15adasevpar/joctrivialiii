<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Jugador;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class JugadorController extends Controller
{

    /**
     * @Route("/crearJugador", name="crearJugador")
     */
    public function crearJugador()
    {        
        $numeroJugador = rand(1,1000);
        $puntuacio = rand(1,100);
        $jugador = new Jugador();
        $jugador->setNom('jugador'.$numeroJugador);
        $jugador->setPuntuacio($puntuacio);
        $jugador->setPosicio(0);

        $em = $this->getDoctrine()->getManager();

        $em->persist($jugador);

        $em->flush();

        return new Response('Nou jugador creat. Id: '.$jugador->getId() . ' Nom: ' . $jugador->getNom() . ' Posició: ' . $jugador->getPosicio());
        
    }

    /**
     * @Route("/inserirJugadorBDD/{nomJugador}{posicioJugador}{puntuacioJugador}", name="inserirJugadorBDD")
     */
    public function inserirJugadorBDD($nomJugador, $posicioJugador, $puntuacioJugador,Request $request)
    {
        $nouJugador = new Jugador();
        $nouJugador->setNom($nomJugador);
        $nouJugador->setPosicio($posicioJugador);
        $nouJugador->setPuntuacio($puntuacioJugador);
        $em = $this->getDoctrine()->getManager();

        $em->persist($nouJugador);

        $em->flush();

        return new Response('Nou jugador creat. Id: '.$nouJugador->getId() . ' Nom: ' . $nouJugador->getNom() . ' Posició: ' . $nouJugador->getPosicio());
    }

    /**
     * @Route("/buscarJugadorBDD/{nomJugador}", name="buscarJugadorBDD")
     */
    public function buscarJugadorBDD($nomJugador,Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb->select('*')
            ->from('jugador','j')
            ->where('j.nom = ?'.$nomJugador);
        if (count($qb) > 0)
            return $this->redirectToRoute('jugarPartida');

        return new Response('Nou jugador creat. Id: '.$nouJugador->getId() . ' Nom: ' . $nouJugador->getNom() . ' Posició: ' . $nouJugador->getPosicio());
    }

    /**
     * @Route("/mostrarJugadors", name="mostrarJugadors")
     */
    public function mostrarJugadors()
    {
        $jugadors = $this->getDoctrine()
            ->getRepository('AppBundle:Jugador')
            ->findAll();

        if (count($jugadors)==0) {
            return $this->render('default/message.html.twig', array('message' => 'No jugadors found'));
        }
        return $this->render('jugador/jugador.html.twig', array(
            'jugadors' => $jugadors));
    }
    /**
     * @Route("/mostrarJugador", name="mostrarJugador")
     */
    /*public function mostrarJugador(Request $request) {
        //Per definir!
    }*/
    
}
