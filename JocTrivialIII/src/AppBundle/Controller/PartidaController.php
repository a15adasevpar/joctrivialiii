<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Jugador;
use AppBundle\Entity\Pregunta;
//use AppBundle\Entity\Partida;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class PartidaController extends Controller
{
    /**
     * @Route("/", name="jugarPartida")
     */
    public function jugarPartida(Request $request)
    {   
        $user = $this->getUser();
        if (!$user) {
            return $this->redirect('login');
        }

        $jugadors = $this->getDoctrine()
            ->getRepository('AppBundle:Jugador')
            ->findAll();
        $preguntes = $this->getDoctrine()
            ->getRepository('AppBundle:Pregunta')
            ->findAll();
        
        //Si no hi ha preguntes -> mostrar només els jugadors
        if (count($preguntes)==0 && count($jugadors)!= 0) {
            return $this->render('partida/partida.html.twig', array('jugadors' => $jugadors));
        }
        //Si no hi ha jugadors -> mostrar només les preguntes
        else if (count($jugadors) == 0 && count($preguntes)!=0) {
            return $this->render('partida/partida.html.twig', array('preguntes' => $preguntes));
        }
        //Si no hi ha ni jugadors ni preguntes -> passar arrays buits
        elseif (count($preguntes)==0 && count($jugadors)== 0) {
            return $this->render('partida/partida.html.twig', array('preguntes','preguntes'));
        }
        //Cas normal: hi han jugadors i preguntes i es mostren els dos
        return $this->render('partida/partida.html.twig', array('jugadors' => $jugadors, 'preguntes' => $preguntes));
        
    }
    /**
     * @Route("/fiPartida", name="fiPartida")
     */
    public function fiPartida(Request $request)
    {   
        $user = $this->getUser();
        if (!$user) {
            return $this->redirect('login');
        }
        
        $respostes = array();
        for ($i=0; $i < $_POST['nombrePreguntes']; $i++) { 
            $respostes[] = $_POST['respostaPregunta'.$i];
        }

        //return $this->render('partida/fiPartida.html.twig',$respostes);

        $resposta0 = $_POST['respostaPregunta0'];
        $resposta1 = $_POST['respostaPregunta1'];
        $resposta3 = $_POST['respostaPregunta2'];
        

        return $this->render('partida/fiPartida.html.twig',array('resposta0' => $resposta0, 'resposta1' => $resposta1, 'resposta2' => $resposta3));
    }

}
