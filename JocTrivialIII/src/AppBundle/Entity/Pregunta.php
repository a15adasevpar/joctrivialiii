<?php
// src/AppBundle/Pregunta.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="pregunta")
 */

class Pregunta 
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;
	/**
	* @ORM\Column(name="enunciat",type="string", length=100)
	*/
	private $enunciat;
	/**
	* @ORM\Column(name="resposta1",type="string", length=100)
	*/
	private $resposta1;
	/**
	* @ORM\Column(name="resposta2",type="string", length=100)
	*/
	private $resposta2;
	/**
	* @ORM\Column(name="resposta3",type="string", length=100)
	*/
	private $resposta3;
	/**
	* @ORM\Column(name="resposta4",type="string", length=100)
	*/
	private $resposta4;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enunciat
     *
     * @param string $enunciat
     *
     * @return Pregunta
     */
    public function setEnunciat($enunciat)
    {
        $this->enunciat = $enunciat;

        return $this;
    }

    /**
     * Get enunciat
     *
     * @return string
     */
    public function getEnunciat()
    {
        return $this->enunciat;
    }

    /**
     * Set resposta1
     *
     * @param string $resposta1
     *
     * @return Pregunta
     */
    public function setResposta1($resposta1)
    {
        $this->resposta1 = $resposta1;

        return $this;
    }

    /**
     * Get resposta1
     *
     * @return string
     */
    public function getResposta1()
    {
        return $this->resposta1;
    }

    /**
     * Set resposta2
     *
     * @param string $resposta2
     *
     * @return Pregunta
     */
    public function setResposta2($resposta2)
    {
        $this->resposta2 = $resposta2;

        return $this;
    }

    /**
     * Get resposta2
     *
     * @return string
     */
    public function getResposta2()
    {
        return $this->resposta2;
    }

    /**
     * Set resposta3
     *
     * @param string $resposta3
     *
     * @return Pregunta
     */
    public function setResposta3($resposta3)
    {
        $this->resposta3 = $resposta3;

        return $this;
    }

    /**
     * Get resposta3
     *
     * @return string
     */
    public function getResposta3()
    {
        return $this->resposta3;
    }

    /**
     * Set resposta4
     *
     * @param string $resposta4
     *
     * @return Pregunta
     */
    public function setResposta4($resposta4)
    {
        $this->resposta4 = $resposta4;

        return $this;
    }

    /**
     * Get resposta4
     *
     * @return string
     */
    public function getResposta4()
    {
        return $this->resposta4;
    }
}
