<?php
// src/AppBundle/Jugador.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="jugador")
 */

class Jugador
{
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;
	
	/**
	* @ORM\Column(name="nom",type="string", length=20)
	*/
	private $nom;

	/**
	* @ORM\Column(name="posicio", type="integer")
	*/
	private $posicio;

	/**
	* @ORM\Column(name="puntuacio", type="integer")
	*/
	private $puntuacio;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Jugador
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set posicio
     *
     * @param integer $posicio
     *
     * @return Jugador
     */
    public function setPosicio($posicio)
    {
        $this->posicio = $posicio;

        return $this;
    }

    /**
     * Get posicio
     *
     * @return integer
     */
    public function getPosicio()
    {
        return $this->posicio;
    }

    /**
     * Set puntuacio
     *
     * @param integer $puntuacio
     *
     * @return Jugador
     */
    public function setPuntuacio($puntuacio)
    {
        $this->puntuacio = $puntuacio;

        return $this;
    }

    /**
     * Get puntuacio
     *
     * @return integer
     */
    public function getPuntuacio()
    {
        return $this->puntuacio;
    }
}
