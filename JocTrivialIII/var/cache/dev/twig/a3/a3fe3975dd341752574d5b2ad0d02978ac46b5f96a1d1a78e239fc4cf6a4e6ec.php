<?php

/* partida/partida.html.twig */
class __TwigTemplate_7c9f9ed746c128c156fd20c7995b414b6af6a5269d27e6080612ec58f865e9e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "partida/partida.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3e607fa22c90de1aac263cfe6e62a4ad6fc8cd81271e0d20e82e473f5f72ed13 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e607fa22c90de1aac263cfe6e62a4ad6fc8cd81271e0d20e82e473f5f72ed13->enter($__internal_3e607fa22c90de1aac263cfe6e62a4ad6fc8cd81271e0d20e82e473f5f72ed13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "partida/partida.html.twig"));

        $__internal_463c7c2d136e369d516d18ba37383b0d4e9bfb7daf7529605d6e9557896cae3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_463c7c2d136e369d516d18ba37383b0d4e9bfb7daf7529605d6e9557896cae3f->enter($__internal_463c7c2d136e369d516d18ba37383b0d4e9bfb7daf7529605d6e9557896cae3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "partida/partida.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3e607fa22c90de1aac263cfe6e62a4ad6fc8cd81271e0d20e82e473f5f72ed13->leave($__internal_3e607fa22c90de1aac263cfe6e62a4ad6fc8cd81271e0d20e82e473f5f72ed13_prof);

        
        $__internal_463c7c2d136e369d516d18ba37383b0d4e9bfb7daf7529605d6e9557896cae3f->leave($__internal_463c7c2d136e369d516d18ba37383b0d4e9bfb7daf7529605d6e9557896cae3f_prof);

    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        $__internal_3bd36dc7586a979371ba588a07eacfd58934f5e4272f02554b4f7b73f17e1d83 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3bd36dc7586a979371ba588a07eacfd58934f5e4272f02554b4f7b73f17e1d83->enter($__internal_3bd36dc7586a979371ba588a07eacfd58934f5e4272f02554b4f7b73f17e1d83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_63dcc92d5ea5a78e63991f8e0c9cc559f80d90cf534a4f2eada37e1df5abdcc1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63dcc92d5ea5a78e63991f8e0c9cc559f80d90cf534a4f2eada37e1df5abdcc1->enter($__internal_63dcc92d5ea5a78e63991f8e0c9cc559f80d90cf534a4f2eada37e1df5abdcc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 3
        echo "    ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 6
        echo "<title>";
        $this->displayBlock('title', $context, $blocks);
        echo " </title>
";
        
        $__internal_63dcc92d5ea5a78e63991f8e0c9cc559f80d90cf534a4f2eada37e1df5abdcc1->leave($__internal_63dcc92d5ea5a78e63991f8e0c9cc559f80d90cf534a4f2eada37e1df5abdcc1_prof);

        
        $__internal_3bd36dc7586a979371ba588a07eacfd58934f5e4272f02554b4f7b73f17e1d83->leave($__internal_3bd36dc7586a979371ba588a07eacfd58934f5e4272f02554b4f7b73f17e1d83_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_dff042e1a14384129a2bb894099086e55e1b34d1ec35793a29981003ec9aa35c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dff042e1a14384129a2bb894099086e55e1b34d1ec35793a29981003ec9aa35c->enter($__internal_dff042e1a14384129a2bb894099086e55e1b34d1ec35793a29981003ec9aa35c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_169a3a9181cdf097d225586c48ae5a2adb64aa0e141f091274eabbd8f804f78b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_169a3a9181cdf097d225586c48ae5a2adb64aa0e141f091274eabbd8f804f78b->enter($__internal_169a3a9181cdf097d225586c48ae5a2adb64aa0e141f091274eabbd8f804f78b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "        <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/partida.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
    ";
        
        $__internal_169a3a9181cdf097d225586c48ae5a2adb64aa0e141f091274eabbd8f804f78b->leave($__internal_169a3a9181cdf097d225586c48ae5a2adb64aa0e141f091274eabbd8f804f78b_prof);

        
        $__internal_dff042e1a14384129a2bb894099086e55e1b34d1ec35793a29981003ec9aa35c->leave($__internal_dff042e1a14384129a2bb894099086e55e1b34d1ec35793a29981003ec9aa35c_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_640f8a57e99cd21360899309a423226e2a58c6f7d3847e0ef0bda06acd85a430 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_640f8a57e99cd21360899309a423226e2a58c6f7d3847e0ef0bda06acd85a430->enter($__internal_640f8a57e99cd21360899309a423226e2a58c6f7d3847e0ef0bda06acd85a430_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1c33f0bf9b0b4703bd935785be3c2158442f1835e1c2251e88288ab3d94868cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c33f0bf9b0b4703bd935785be3c2158442f1835e1c2251e88288ab3d94868cc->enter($__internal_1c33f0bf9b0b4703bd935785be3c2158442f1835e1c2251e88288ab3d94868cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Trivial Pursuit";
        
        $__internal_1c33f0bf9b0b4703bd935785be3c2158442f1835e1c2251e88288ab3d94868cc->leave($__internal_1c33f0bf9b0b4703bd935785be3c2158442f1835e1c2251e88288ab3d94868cc_prof);

        
        $__internal_640f8a57e99cd21360899309a423226e2a58c6f7d3847e0ef0bda06acd85a430->leave($__internal_640f8a57e99cd21360899309a423226e2a58c6f7d3847e0ef0bda06acd85a430_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_0ab6db7a5eef0b086b14c482852363fb8effb8311213bba63a36fb1a91feaea9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ab6db7a5eef0b086b14c482852363fb8effb8311213bba63a36fb1a91feaea9->enter($__internal_0ab6db7a5eef0b086b14c482852363fb8effb8311213bba63a36fb1a91feaea9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1eb6cf07342b198f26cfa47e9c66c1465c1f1a23d75d90a608b07305d7b731a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1eb6cf07342b198f26cfa47e9c66c1465c1f1a23d75d90a608b07305d7b731a0->enter($__internal_1eb6cf07342b198f26cfa47e9c66c1465c1f1a23d75d90a608b07305d7b731a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 9
        echo "    <h2> Partida Trivial </h2>
        ";
        // line 10
        if (array_key_exists("preguntes", $context)) {
            // line 11
            echo "            <form id=\"formPartida\" name=\"formPartida\" action=\"/fiPartida\" method=\"post\">
                <h3> Contesta les preguntes </h3>
                <div class=\"container\">
                ";
            // line 14
            $context["i"] = 0;
            // line 15
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["preguntes"] ?? $this->getContext($context, "preguntes")));
            foreach ($context['_seq'] as $context["_key"] => $context["pregunta"]) {
                // line 16
                echo "                    <h4>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["pregunta"], "enunciat", array()), "html", null, true);
                echo "</h4>
                    <ul>
                        ";
                // line 24
                echo "
                        <li>
                            <input type=\"radio\" id=\"f-option";
                // line 26
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\" name=\"respostaPregunta";
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\" value=\"correct\">
                            <label for=\"f-option";
                // line 27
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["pregunta"], "resposta1", array()), "html", null, true);
                echo "</label>
                        <div class=\"check\"></div>
                        </li>

                        <li>
                            <input type=\"radio\" id=\"s-option";
                // line 32
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\" name=\"respostaPregunta";
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\" value=\"wrong\">
                            <label for=\"s-option";
                // line 33
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["pregunta"], "resposta2", array()), "html", null, true);
                echo "</label>
                        <div class=\"check\"><div class=\"inside\"></div></div>
                        </li>

                        <li>
                            <input type=\"radio\" id=\"t-option";
                // line 38
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\" name=\"respostaPregunta";
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\" value=\"wrong\">
                            <label for=\"t-option";
                // line 39
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["pregunta"], "resposta3", array()), "html", null, true);
                echo "</label>
                        <div class=\"check\"><div class=\"inside\"></div></div>
                        </li>
                        
                        <li>
                            <input type=\"radio\" id=\"n-option";
                // line 44
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\" name=\"respostaPregunta";
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\" value=\"wrong\">
                            <label for=\"n-option";
                // line 45
                echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["pregunta"], "resposta4", array()), "html", null, true);
                echo "</label>
                        <div class=\"check\"><div class=\"inside\"></div></div>
                        </li>
  
                    </ul>
                    ";
                // line 50
                $context["i"] = (($context["i"] ?? $this->getContext($context, "i")) + 1);
                // line 51
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pregunta'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "                </div>
                <input type=\"hidden\" id=\"nombrePreguntes\" name=\"nombrePreguntes\" value=\"";
            // line 53
            echo twig_escape_filter($this->env, ($context["i"] ?? $this->getContext($context, "i")), "html", null, true);
            echo "\">
                <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Enviar respostes"), "html", null, true);
            echo "\" />
            </form>
        ";
        } else {
            // line 57
            echo "            <p>No hi han preguntes registrades</p>
            <a href=\"./crearPregunta\">Registre</a>
        ";
        }
        
        $__internal_1eb6cf07342b198f26cfa47e9c66c1465c1f1a23d75d90a608b07305d7b731a0->leave($__internal_1eb6cf07342b198f26cfa47e9c66c1465c1f1a23d75d90a608b07305d7b731a0_prof);

        
        $__internal_0ab6db7a5eef0b086b14c482852363fb8effb8311213bba63a36fb1a91feaea9->leave($__internal_0ab6db7a5eef0b086b14c482852363fb8effb8311213bba63a36fb1a91feaea9_prof);

    }

    public function getTemplateName()
    {
        return "partida/partida.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  229 => 57,  223 => 54,  219 => 53,  216 => 52,  210 => 51,  208 => 50,  198 => 45,  192 => 44,  182 => 39,  176 => 38,  166 => 33,  160 => 32,  150 => 27,  144 => 26,  140 => 24,  134 => 16,  129 => 15,  127 => 14,  122 => 11,  120 => 10,  117 => 9,  108 => 8,  90 => 6,  77 => 4,  68 => 3,  55 => 6,  52 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block head %}
    {% block stylesheets %}
        <link href=\"{{ asset('css/partida.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
    {% endblock %}
<title>{% block title %}Trivial Pursuit{% endblock %} </title>
{% endblock %}
{% block body %}
    <h2> Partida Trivial </h2>
        {% if preguntes is defined %}
            <form id=\"formPartida\" name=\"formPartida\" action=\"/fiPartida\" method=\"post\">
                <h3> Contesta les preguntes </h3>
                <div class=\"container\">
                {% set i = 0 %}
                {% for pregunta in preguntes %}
                    <h4>{{ pregunta.enunciat }}</h4>
                    <ul>
                        {#
                        <li><input type=\"radio\" name=\"respostaPregunta{{ i }}\" value=\"correct\">{{ pregunta.resposta1 }}</li>
                        <li><input type=\"radio\" name=\"respostaPregunta{{ i }}\" value=\"wrong\">{{ pregunta.resposta2 }}</li>
                        <li><input type=\"radio\" name=\"respostaPregunta{{ i }}\" value=\"wrong\">{{ pregunta.resposta3 }}</li>
                        <li><input type=\"radio\" name=\"respostaPregunta{{ i }}\" value=\"wrong\">{{ pregunta.resposta4 }}</li>
                        #}

                        <li>
                            <input type=\"radio\" id=\"f-option{{i}}\" name=\"respostaPregunta{{ i }}\" value=\"correct\">
                            <label for=\"f-option{{i}}\">{{ pregunta.resposta1 }}</label>
                        <div class=\"check\"></div>
                        </li>

                        <li>
                            <input type=\"radio\" id=\"s-option{{i}}\" name=\"respostaPregunta{{ i }}\" value=\"wrong\">
                            <label for=\"s-option{{i}}\">{{ pregunta.resposta2 }}</label>
                        <div class=\"check\"><div class=\"inside\"></div></div>
                        </li>

                        <li>
                            <input type=\"radio\" id=\"t-option{{i}}\" name=\"respostaPregunta{{ i }}\" value=\"wrong\">
                            <label for=\"t-option{{i}}\">{{ pregunta.resposta3 }}</label>
                        <div class=\"check\"><div class=\"inside\"></div></div>
                        </li>
                        
                        <li>
                            <input type=\"radio\" id=\"n-option{{i}}\" name=\"respostaPregunta{{ i }}\" value=\"wrong\">
                            <label for=\"n-option{{i}}\">{{ pregunta.resposta4 }}</label>
                        <div class=\"check\"><div class=\"inside\"></div></div>
                        </li>
  
                    </ul>
                    {% set i = i + 1 %}
                {% endfor %}
                </div>
                <input type=\"hidden\" id=\"nombrePreguntes\" name=\"nombrePreguntes\" value=\"{{ i }}\">
                <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"{{ 'Enviar respostes'|trans }}\" />
            </form>
        {% else %}
            <p>No hi han preguntes registrades</p>
            <a href=\"./crearPregunta\">Registre</a>
        {% endif %}
{% endblock %}", "partida/partida.html.twig", "/home/ausias/Escriptori/projectes_symfony/joctrivialiii/JocTrivialIII/app/Resources/views/partida/partida.html.twig");
    }
}
