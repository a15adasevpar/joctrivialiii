<?php

/* @FOSUser/Registration/confirmed.html.twig */
class __TwigTemplate_fc798587f5db9024bc481f64274ea504183d89f62fe124609595832b72c8d6c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6743c05ebba1c4f918548b55509f7c1bde153fa072a4b22dbce230f13b31be52 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6743c05ebba1c4f918548b55509f7c1bde153fa072a4b22dbce230f13b31be52->enter($__internal_6743c05ebba1c4f918548b55509f7c1bde153fa072a4b22dbce230f13b31be52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $__internal_e83b3fdbfaa61b289d31b2bc66abab8122e526b1723ed48895df758902bc6f18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e83b3fdbfaa61b289d31b2bc66abab8122e526b1723ed48895df758902bc6f18->enter($__internal_e83b3fdbfaa61b289d31b2bc66abab8122e526b1723ed48895df758902bc6f18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6743c05ebba1c4f918548b55509f7c1bde153fa072a4b22dbce230f13b31be52->leave($__internal_6743c05ebba1c4f918548b55509f7c1bde153fa072a4b22dbce230f13b31be52_prof);

        
        $__internal_e83b3fdbfaa61b289d31b2bc66abab8122e526b1723ed48895df758902bc6f18->leave($__internal_e83b3fdbfaa61b289d31b2bc66abab8122e526b1723ed48895df758902bc6f18_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9ceeb99f9851255c0be652bfba2fbd139de9e0926cbf973682c46001256f16b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ceeb99f9851255c0be652bfba2fbd139de9e0926cbf973682c46001256f16b9->enter($__internal_9ceeb99f9851255c0be652bfba2fbd139de9e0926cbf973682c46001256f16b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_09c1bc7bc86784e7f38258e975b4e28f7cca12b385b8780f8c3fdad0558911a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09c1bc7bc86784e7f38258e975b4e28f7cca12b385b8780f8c3fdad0558911a4->enter($__internal_09c1bc7bc86784e7f38258e975b4e28f7cca12b385b8780f8c3fdad0558911a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if (($context["targetUrl"] ?? $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, ($context["targetUrl"] ?? $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_09c1bc7bc86784e7f38258e975b4e28f7cca12b385b8780f8c3fdad0558911a4->leave($__internal_09c1bc7bc86784e7f38258e975b4e28f7cca12b385b8780f8c3fdad0558911a4_prof);

        
        $__internal_9ceeb99f9851255c0be652bfba2fbd139de9e0926cbf973682c46001256f16b9->leave($__internal_9ceeb99f9851255c0be652bfba2fbd139de9e0926cbf973682c46001256f16b9_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/confirmed.html.twig", "/home/ausias/Escriptori/projectes_symfony/JocTrivialIII/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
