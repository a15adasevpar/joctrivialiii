<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_12f738d54aff3bf3f6a56dc81f2505fdcba5d762d3d028c6a50f1a44ad74da5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a74a642f53fc621d9df8d2f45c19cd8e6bec523bece1f98d43e00d3aebbd8fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a74a642f53fc621d9df8d2f45c19cd8e6bec523bece1f98d43e00d3aebbd8fd->enter($__internal_8a74a642f53fc621d9df8d2f45c19cd8e6bec523bece1f98d43e00d3aebbd8fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_f3a5a0ffa6c82417ba19cc188cb7aa1bfd838bc80e2bda4142b9f14efaa0c2f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3a5a0ffa6c82417ba19cc188cb7aa1bfd838bc80e2bda4142b9f14efaa0c2f7->enter($__internal_f3a5a0ffa6c82417ba19cc188cb7aa1bfd838bc80e2bda4142b9f14efaa0c2f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8a74a642f53fc621d9df8d2f45c19cd8e6bec523bece1f98d43e00d3aebbd8fd->leave($__internal_8a74a642f53fc621d9df8d2f45c19cd8e6bec523bece1f98d43e00d3aebbd8fd_prof);

        
        $__internal_f3a5a0ffa6c82417ba19cc188cb7aa1bfd838bc80e2bda4142b9f14efaa0c2f7->leave($__internal_f3a5a0ffa6c82417ba19cc188cb7aa1bfd838bc80e2bda4142b9f14efaa0c2f7_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_012ef325200fc37cf68bce1075b8a3bd8d16e8c04e0466a54532fcaf2533c6f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_012ef325200fc37cf68bce1075b8a3bd8d16e8c04e0466a54532fcaf2533c6f6->enter($__internal_012ef325200fc37cf68bce1075b8a3bd8d16e8c04e0466a54532fcaf2533c6f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_d6a8624242720e75d9ed5caa6374c95db939ee7d89425cd0a5fba004489481e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6a8624242720e75d9ed5caa6374c95db939ee7d89425cd0a5fba004489481e5->enter($__internal_d6a8624242720e75d9ed5caa6374c95db939ee7d89425cd0a5fba004489481e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_d6a8624242720e75d9ed5caa6374c95db939ee7d89425cd0a5fba004489481e5->leave($__internal_d6a8624242720e75d9ed5caa6374c95db939ee7d89425cd0a5fba004489481e5_prof);

        
        $__internal_012ef325200fc37cf68bce1075b8a3bd8d16e8c04e0466a54532fcaf2533c6f6->leave($__internal_012ef325200fc37cf68bce1075b8a3bd8d16e8c04e0466a54532fcaf2533c6f6_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_c01faeba4195d599a368612faaf619cfc7822b824bcecb6e69776162ea155bb0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c01faeba4195d599a368612faaf619cfc7822b824bcecb6e69776162ea155bb0->enter($__internal_c01faeba4195d599a368612faaf619cfc7822b824bcecb6e69776162ea155bb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_808e9fcf11fadca204dfb81a9cb52999cc9fb3059c71def220fcc3e774c85948 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_808e9fcf11fadca204dfb81a9cb52999cc9fb3059c71def220fcc3e774c85948->enter($__internal_808e9fcf11fadca204dfb81a9cb52999cc9fb3059c71def220fcc3e774c85948_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_808e9fcf11fadca204dfb81a9cb52999cc9fb3059c71def220fcc3e774c85948->leave($__internal_808e9fcf11fadca204dfb81a9cb52999cc9fb3059c71def220fcc3e774c85948_prof);

        
        $__internal_c01faeba4195d599a368612faaf619cfc7822b824bcecb6e69776162ea155bb0->leave($__internal_c01faeba4195d599a368612faaf619cfc7822b824bcecb6e69776162ea155bb0_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_2e2770c4d7f4e60531751b75cc8be4280ea7285b57cbc831f8fbac9b209da71e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e2770c4d7f4e60531751b75cc8be4280ea7285b57cbc831f8fbac9b209da71e->enter($__internal_2e2770c4d7f4e60531751b75cc8be4280ea7285b57cbc831f8fbac9b209da71e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_d00274f3be815e4e878a9eba2397166c34d692590f99a90a58e137cfdf389749 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d00274f3be815e4e878a9eba2397166c34d692590f99a90a58e137cfdf389749->enter($__internal_d00274f3be815e4e878a9eba2397166c34d692590f99a90a58e137cfdf389749_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_d00274f3be815e4e878a9eba2397166c34d692590f99a90a58e137cfdf389749->leave($__internal_d00274f3be815e4e878a9eba2397166c34d692590f99a90a58e137cfdf389749_prof);

        
        $__internal_2e2770c4d7f4e60531751b75cc8be4280ea7285b57cbc831f8fbac9b209da71e->leave($__internal_2e2770c4d7f4e60531751b75cc8be4280ea7285b57cbc831f8fbac9b209da71e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/ausias/Escriptori/projectes_symfony/joctrivialiii/JocTrivialIII/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
