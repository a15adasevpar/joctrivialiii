<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_f5a4e4e47a61dbc596b8a5cf6c4251cf9809cbaa7fe0192c935bf3aa4bd7f2e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b6e8754dc53f7d77a086eaeb14bf4069207b8ba6056e8fade2cd2a00c1ce4357 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6e8754dc53f7d77a086eaeb14bf4069207b8ba6056e8fade2cd2a00c1ce4357->enter($__internal_b6e8754dc53f7d77a086eaeb14bf4069207b8ba6056e8fade2cd2a00c1ce4357_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_69d1c250fd83df05379491996b32c0c8e3b14d4af5918e4499ac5de4e1e65643 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69d1c250fd83df05379491996b32c0c8e3b14d4af5918e4499ac5de4e1e65643->enter($__internal_69d1c250fd83df05379491996b32c0c8e3b14d4af5918e4499ac5de4e1e65643_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b6e8754dc53f7d77a086eaeb14bf4069207b8ba6056e8fade2cd2a00c1ce4357->leave($__internal_b6e8754dc53f7d77a086eaeb14bf4069207b8ba6056e8fade2cd2a00c1ce4357_prof);

        
        $__internal_69d1c250fd83df05379491996b32c0c8e3b14d4af5918e4499ac5de4e1e65643->leave($__internal_69d1c250fd83df05379491996b32c0c8e3b14d4af5918e4499ac5de4e1e65643_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e7a99361b4086b372c13d17b892e1a10e9e5c64bb0c1625f85450770e011e15e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e7a99361b4086b372c13d17b892e1a10e9e5c64bb0c1625f85450770e011e15e->enter($__internal_e7a99361b4086b372c13d17b892e1a10e9e5c64bb0c1625f85450770e011e15e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_d707e5a716125f9fb6bda3390e9b90a1043a0520ee8d6c0a8ce0c945e40aa6d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d707e5a716125f9fb6bda3390e9b90a1043a0520ee8d6c0a8ce0c945e40aa6d1->enter($__internal_d707e5a716125f9fb6bda3390e9b90a1043a0520ee8d6c0a8ce0c945e40aa6d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_d707e5a716125f9fb6bda3390e9b90a1043a0520ee8d6c0a8ce0c945e40aa6d1->leave($__internal_d707e5a716125f9fb6bda3390e9b90a1043a0520ee8d6c0a8ce0c945e40aa6d1_prof);

        
        $__internal_e7a99361b4086b372c13d17b892e1a10e9e5c64bb0c1625f85450770e011e15e->leave($__internal_e7a99361b4086b372c13d17b892e1a10e9e5c64bb0c1625f85450770e011e15e_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_2d9ff085bcf6447d7ecbee6dffe34dcc07e3c9ac51fcdf051801b89f21696bc6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d9ff085bcf6447d7ecbee6dffe34dcc07e3c9ac51fcdf051801b89f21696bc6->enter($__internal_2d9ff085bcf6447d7ecbee6dffe34dcc07e3c9ac51fcdf051801b89f21696bc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_0787dce8d509197d12a1f21c3d5fd1c47e21e56cfa2283e0f9b3059fbcd8a76e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0787dce8d509197d12a1f21c3d5fd1c47e21e56cfa2283e0f9b3059fbcd8a76e->enter($__internal_0787dce8d509197d12a1f21c3d5fd1c47e21e56cfa2283e0f9b3059fbcd8a76e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_0787dce8d509197d12a1f21c3d5fd1c47e21e56cfa2283e0f9b3059fbcd8a76e->leave($__internal_0787dce8d509197d12a1f21c3d5fd1c47e21e56cfa2283e0f9b3059fbcd8a76e_prof);

        
        $__internal_2d9ff085bcf6447d7ecbee6dffe34dcc07e3c9ac51fcdf051801b89f21696bc6->leave($__internal_2d9ff085bcf6447d7ecbee6dffe34dcc07e3c9ac51fcdf051801b89f21696bc6_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_b149d4e37e89c3a0b6d1374f39886a4b3b1674b403c2b9d9bf76636ab1bc6f13 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b149d4e37e89c3a0b6d1374f39886a4b3b1674b403c2b9d9bf76636ab1bc6f13->enter($__internal_b149d4e37e89c3a0b6d1374f39886a4b3b1674b403c2b9d9bf76636ab1bc6f13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9f6fa5ba803b9d96ba130f3a493fb5b515f83cb99942a8bbdaa4a920667d2203 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f6fa5ba803b9d96ba130f3a493fb5b515f83cb99942a8bbdaa4a920667d2203->enter($__internal_9f6fa5ba803b9d96ba130f3a493fb5b515f83cb99942a8bbdaa4a920667d2203_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_9f6fa5ba803b9d96ba130f3a493fb5b515f83cb99942a8bbdaa4a920667d2203->leave($__internal_9f6fa5ba803b9d96ba130f3a493fb5b515f83cb99942a8bbdaa4a920667d2203_prof);

        
        $__internal_b149d4e37e89c3a0b6d1374f39886a4b3b1674b403c2b9d9bf76636ab1bc6f13->leave($__internal_b149d4e37e89c3a0b6d1374f39886a4b3b1674b403c2b9d9bf76636ab1bc6f13_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/ausias/Escriptori/projectes_symfony/joctrivialiii/JocTrivialIII/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
