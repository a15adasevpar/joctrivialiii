<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_f902452bd4e2b5bd61051f47431cb025fc906d0603e67cac5f83a0eb7cfdef60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3df95e9b8f11d337bf8c8ccce720171af272836d152233e8ba87205fbe432e83 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3df95e9b8f11d337bf8c8ccce720171af272836d152233e8ba87205fbe432e83->enter($__internal_3df95e9b8f11d337bf8c8ccce720171af272836d152233e8ba87205fbe432e83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_7a6a95cd357264754e2eb87a5694e468e10219b6a8c7f7c53c69b83f188becfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a6a95cd357264754e2eb87a5694e468e10219b6a8c7f7c53c69b83f188becfc->enter($__internal_7a6a95cd357264754e2eb87a5694e468e10219b6a8c7f7c53c69b83f188becfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3df95e9b8f11d337bf8c8ccce720171af272836d152233e8ba87205fbe432e83->leave($__internal_3df95e9b8f11d337bf8c8ccce720171af272836d152233e8ba87205fbe432e83_prof);

        
        $__internal_7a6a95cd357264754e2eb87a5694e468e10219b6a8c7f7c53c69b83f188becfc->leave($__internal_7a6a95cd357264754e2eb87a5694e468e10219b6a8c7f7c53c69b83f188becfc_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9b879fcdd1849d4b7392a077ef5df6d28167a6a5acfb969137a45835154ba7c2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b879fcdd1849d4b7392a077ef5df6d28167a6a5acfb969137a45835154ba7c2->enter($__internal_9b879fcdd1849d4b7392a077ef5df6d28167a6a5acfb969137a45835154ba7c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_605d5f977b5af233bf315ebb39bf659997d159a5805299f81e559e4a59d02ff3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_605d5f977b5af233bf315ebb39bf659997d159a5805299f81e559e4a59d02ff3->enter($__internal_605d5f977b5af233bf315ebb39bf659997d159a5805299f81e559e4a59d02ff3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_605d5f977b5af233bf315ebb39bf659997d159a5805299f81e559e4a59d02ff3->leave($__internal_605d5f977b5af233bf315ebb39bf659997d159a5805299f81e559e4a59d02ff3_prof);

        
        $__internal_9b879fcdd1849d4b7392a077ef5df6d28167a6a5acfb969137a45835154ba7c2->leave($__internal_9b879fcdd1849d4b7392a077ef5df6d28167a6a5acfb969137a45835154ba7c2_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "/home/ausias/Escriptori/projectes_symfony/joctrivialiii/JocTrivialIII/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}
