<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_3bbb6c6803d3f6a382be84877d6b3883a0ab65186a35fac3f7b35f8c4c47b064 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b791328a6feffabc45a654c4d19e2316cce80e0a9640ed4afc2299da9a7aa803 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b791328a6feffabc45a654c4d19e2316cce80e0a9640ed4afc2299da9a7aa803->enter($__internal_b791328a6feffabc45a654c4d19e2316cce80e0a9640ed4afc2299da9a7aa803_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $__internal_9d3dea242fabd655aca41f09837ddd1c89726aaebaf7918a1ccc5acffe13629d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d3dea242fabd655aca41f09837ddd1c89726aaebaf7918a1ccc5acffe13629d->enter($__internal_9d3dea242fabd655aca41f09837ddd1c89726aaebaf7918a1ccc5acffe13629d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b791328a6feffabc45a654c4d19e2316cce80e0a9640ed4afc2299da9a7aa803->leave($__internal_b791328a6feffabc45a654c4d19e2316cce80e0a9640ed4afc2299da9a7aa803_prof);

        
        $__internal_9d3dea242fabd655aca41f09837ddd1c89726aaebaf7918a1ccc5acffe13629d->leave($__internal_9d3dea242fabd655aca41f09837ddd1c89726aaebaf7918a1ccc5acffe13629d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b277c62b8bfe794bbe8ea248e619c33c5dc9586684f4d381768d9fe113da072e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b277c62b8bfe794bbe8ea248e619c33c5dc9586684f4d381768d9fe113da072e->enter($__internal_b277c62b8bfe794bbe8ea248e619c33c5dc9586684f4d381768d9fe113da072e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_d1d1535f0565bcc7b4ed93e3d24e7ea96d6745d9259564400c21d62344dfa47a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1d1535f0565bcc7b4ed93e3d24e7ea96d6745d9259564400c21d62344dfa47a->enter($__internal_d1d1535f0565bcc7b4ed93e3d24e7ea96d6745d9259564400c21d62344dfa47a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_d1d1535f0565bcc7b4ed93e3d24e7ea96d6745d9259564400c21d62344dfa47a->leave($__internal_d1d1535f0565bcc7b4ed93e3d24e7ea96d6745d9259564400c21d62344dfa47a_prof);

        
        $__internal_b277c62b8bfe794bbe8ea248e619c33c5dc9586684f4d381768d9fe113da072e->leave($__internal_b277c62b8bfe794bbe8ea248e619c33c5dc9586684f4d381768d9fe113da072e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/register.html.twig", "/home/ausias/Escriptori/projectes_symfony/JocTrivialIII/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}
