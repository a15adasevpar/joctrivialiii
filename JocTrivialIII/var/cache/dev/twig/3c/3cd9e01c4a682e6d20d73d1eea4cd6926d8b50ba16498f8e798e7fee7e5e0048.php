<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_490088ef8c2d536ba9aa3e4da532e3c9d5d6875dde7bb492995d7d77f4e311f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_88444e2ebfd9a0b1b5c32e680ebfe10ea5f51c2c15a4de4e5ad2528a2d64274c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_88444e2ebfd9a0b1b5c32e680ebfe10ea5f51c2c15a4de4e5ad2528a2d64274c->enter($__internal_88444e2ebfd9a0b1b5c32e680ebfe10ea5f51c2c15a4de4e5ad2528a2d64274c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_7d8c195da6255a4a7e32dbfdabd3b727bef4c6451a9c50cbdb07c7a54ed39edb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d8c195da6255a4a7e32dbfdabd3b727bef4c6451a9c50cbdb07c7a54ed39edb->enter($__internal_7d8c195da6255a4a7e32dbfdabd3b727bef4c6451a9c50cbdb07c7a54ed39edb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_88444e2ebfd9a0b1b5c32e680ebfe10ea5f51c2c15a4de4e5ad2528a2d64274c->leave($__internal_88444e2ebfd9a0b1b5c32e680ebfe10ea5f51c2c15a4de4e5ad2528a2d64274c_prof);

        
        $__internal_7d8c195da6255a4a7e32dbfdabd3b727bef4c6451a9c50cbdb07c7a54ed39edb->leave($__internal_7d8c195da6255a4a7e32dbfdabd3b727bef4c6451a9c50cbdb07c7a54ed39edb_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_91c6802c2959e87e29dd49189cb7f8c16a0348f35163c12db8efc700b52c86ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_91c6802c2959e87e29dd49189cb7f8c16a0348f35163c12db8efc700b52c86ee->enter($__internal_91c6802c2959e87e29dd49189cb7f8c16a0348f35163c12db8efc700b52c86ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_011d3849105b8bb0e83ef2b911bdd2452cd2f546ded9e6588e8611704d06cdf7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_011d3849105b8bb0e83ef2b911bdd2452cd2f546ded9e6588e8611704d06cdf7->enter($__internal_011d3849105b8bb0e83ef2b911bdd2452cd2f546ded9e6588e8611704d06cdf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_011d3849105b8bb0e83ef2b911bdd2452cd2f546ded9e6588e8611704d06cdf7->leave($__internal_011d3849105b8bb0e83ef2b911bdd2452cd2f546ded9e6588e8611704d06cdf7_prof);

        
        $__internal_91c6802c2959e87e29dd49189cb7f8c16a0348f35163c12db8efc700b52c86ee->leave($__internal_91c6802c2959e87e29dd49189cb7f8c16a0348f35163c12db8efc700b52c86ee_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_2628cedc928fb1aaa7af80c206946db4f890930d1cbbe8b95a4e84cb8b5a4088 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2628cedc928fb1aaa7af80c206946db4f890930d1cbbe8b95a4e84cb8b5a4088->enter($__internal_2628cedc928fb1aaa7af80c206946db4f890930d1cbbe8b95a4e84cb8b5a4088_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_bf6d3935fdd146a780b8147d44e1f74d72a9bce139e6f72036c5343b5dde49c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf6d3935fdd146a780b8147d44e1f74d72a9bce139e6f72036c5343b5dde49c5->enter($__internal_bf6d3935fdd146a780b8147d44e1f74d72a9bce139e6f72036c5343b5dde49c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_bf6d3935fdd146a780b8147d44e1f74d72a9bce139e6f72036c5343b5dde49c5->leave($__internal_bf6d3935fdd146a780b8147d44e1f74d72a9bce139e6f72036c5343b5dde49c5_prof);

        
        $__internal_2628cedc928fb1aaa7af80c206946db4f890930d1cbbe8b95a4e84cb8b5a4088->leave($__internal_2628cedc928fb1aaa7af80c206946db4f890930d1cbbe8b95a4e84cb8b5a4088_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_2a06e72a1ce2cbffff96d771b13d7a70d82306f8a13a4221b3694edac70e60b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a06e72a1ce2cbffff96d771b13d7a70d82306f8a13a4221b3694edac70e60b6->enter($__internal_2a06e72a1ce2cbffff96d771b13d7a70d82306f8a13a4221b3694edac70e60b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_175a323be43132b351aa2ff3980805607f8a99e4aed29592f93dc9009d0e34ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_175a323be43132b351aa2ff3980805607f8a99e4aed29592f93dc9009d0e34ae->enter($__internal_175a323be43132b351aa2ff3980805607f8a99e4aed29592f93dc9009d0e34ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_175a323be43132b351aa2ff3980805607f8a99e4aed29592f93dc9009d0e34ae->leave($__internal_175a323be43132b351aa2ff3980805607f8a99e4aed29592f93dc9009d0e34ae_prof);

        
        $__internal_2a06e72a1ce2cbffff96d771b13d7a70d82306f8a13a4221b3694edac70e60b6->leave($__internal_2a06e72a1ce2cbffff96d771b13d7a70d82306f8a13a4221b3694edac70e60b6_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/ausias/Escriptori/projectes_symfony/joctrivialiii/JocTrivialIII/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
