<?php

/* portada/portada.html.twig */
class __TwigTemplate_bae4df4cf513bc8e110691256479777ed30c19eb378ef16909df9eb39f4f082f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "portada/portada.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_44f939d932a3734f429699122c3cf37f890cb698c0f46bbca5cf1ebca5522ea1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44f939d932a3734f429699122c3cf37f890cb698c0f46bbca5cf1ebca5522ea1->enter($__internal_44f939d932a3734f429699122c3cf37f890cb698c0f46bbca5cf1ebca5522ea1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "portada/portada.html.twig"));

        $__internal_80ae1dc18ff803b40b8430c2fd15b90ebbeee2d82a1f2bc9b90b7a295e2f3368 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80ae1dc18ff803b40b8430c2fd15b90ebbeee2d82a1f2bc9b90b7a295e2f3368->enter($__internal_80ae1dc18ff803b40b8430c2fd15b90ebbeee2d82a1f2bc9b90b7a295e2f3368_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "portada/portada.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_44f939d932a3734f429699122c3cf37f890cb698c0f46bbca5cf1ebca5522ea1->leave($__internal_44f939d932a3734f429699122c3cf37f890cb698c0f46bbca5cf1ebca5522ea1_prof);

        
        $__internal_80ae1dc18ff803b40b8430c2fd15b90ebbeee2d82a1f2bc9b90b7a295e2f3368->leave($__internal_80ae1dc18ff803b40b8430c2fd15b90ebbeee2d82a1f2bc9b90b7a295e2f3368_prof);

    }

    // line 2
    public function block_head($context, array $blocks = array())
    {
        $__internal_20ae9e51da89bd03fa4be2857f771ca504f226808479066dfb6493d505011955 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_20ae9e51da89bd03fa4be2857f771ca504f226808479066dfb6493d505011955->enter($__internal_20ae9e51da89bd03fa4be2857f771ca504f226808479066dfb6493d505011955_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_a67701eedb180552947585d43ca4783ffa9a7f36e946b53206fa8c5971a28cdc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a67701eedb180552947585d43ca4783ffa9a7f36e946b53206fa8c5971a28cdc->enter($__internal_a67701eedb180552947585d43ca4783ffa9a7f36e946b53206fa8c5971a28cdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 3
        echo "    ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 6
        echo "<title>";
        $this->displayBlock('title', $context, $blocks);
        echo " </title>
";
        
        $__internal_a67701eedb180552947585d43ca4783ffa9a7f36e946b53206fa8c5971a28cdc->leave($__internal_a67701eedb180552947585d43ca4783ffa9a7f36e946b53206fa8c5971a28cdc_prof);

        
        $__internal_20ae9e51da89bd03fa4be2857f771ca504f226808479066dfb6493d505011955->leave($__internal_20ae9e51da89bd03fa4be2857f771ca504f226808479066dfb6493d505011955_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_31446528b78e73b56b228966e59fd6d7d03b48416138fec0245f4ae64088f6a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_31446528b78e73b56b228966e59fd6d7d03b48416138fec0245f4ae64088f6a7->enter($__internal_31446528b78e73b56b228966e59fd6d7d03b48416138fec0245f4ae64088f6a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_a70aa0c4bfadbcd05e1023f22b2cc25e4f64f09de53bd660cf10490bbbc2ba37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a70aa0c4bfadbcd05e1023f22b2cc25e4f64f09de53bd660cf10490bbbc2ba37->enter($__internal_a70aa0c4bfadbcd05e1023f22b2cc25e4f64f09de53bd660cf10490bbbc2ba37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "        <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/portada.css"), "html", null, true);
        echo "\" type=\"text/css\" rel=\"stylesheet\" />
    ";
        
        $__internal_a70aa0c4bfadbcd05e1023f22b2cc25e4f64f09de53bd660cf10490bbbc2ba37->leave($__internal_a70aa0c4bfadbcd05e1023f22b2cc25e4f64f09de53bd660cf10490bbbc2ba37_prof);

        
        $__internal_31446528b78e73b56b228966e59fd6d7d03b48416138fec0245f4ae64088f6a7->leave($__internal_31446528b78e73b56b228966e59fd6d7d03b48416138fec0245f4ae64088f6a7_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_f010d223ee2c0475a3c3c729b0ce393bf4d00446b3ed96b575e81d3bead66b62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f010d223ee2c0475a3c3c729b0ce393bf4d00446b3ed96b575e81d3bead66b62->enter($__internal_f010d223ee2c0475a3c3c729b0ce393bf4d00446b3ed96b575e81d3bead66b62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_650bbdf6c1a6b182d5cce1ed7ac32b438acc81e830bacc418b03af75a21dbb5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_650bbdf6c1a6b182d5cce1ed7ac32b438acc81e830bacc418b03af75a21dbb5c->enter($__internal_650bbdf6c1a6b182d5cce1ed7ac32b438acc81e830bacc418b03af75a21dbb5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Trivial Pursuit";
        
        $__internal_650bbdf6c1a6b182d5cce1ed7ac32b438acc81e830bacc418b03af75a21dbb5c->leave($__internal_650bbdf6c1a6b182d5cce1ed7ac32b438acc81e830bacc418b03af75a21dbb5c_prof);

        
        $__internal_f010d223ee2c0475a3c3c729b0ce393bf4d00446b3ed96b575e81d3bead66b62->leave($__internal_f010d223ee2c0475a3c3c729b0ce393bf4d00446b3ed96b575e81d3bead66b62_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_5854d2fc19ecf802bee338c9f0795e9af155c85a459b0c7e513c7cbd6ead0a12 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5854d2fc19ecf802bee338c9f0795e9af155c85a459b0c7e513c7cbd6ead0a12->enter($__internal_5854d2fc19ecf802bee338c9f0795e9af155c85a459b0c7e513c7cbd6ead0a12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c23b2046682b892c4a4a5967e1d296f2b505b709d56321b5cab59b62bf8280c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c23b2046682b892c4a4a5967e1d296f2b505b709d56321b5cab59b62bf8280c4->enter($__internal_c23b2046682b892c4a4a5967e1d296f2b505b709d56321b5cab59b62bf8280c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 9
        echo "    <h3> Portada Trivial </h3>
        ";
        // line 10
        if (array_key_exists("jugadors", $context)) {
            // line 11
            echo "        <p>Aquest portal et permet jugar contestant una sèrie de preguntes i obtenint una puntuació</p>
            ";
            // line 12
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
            echo "
            ";
            // line 13
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
            echo "
            ";
            // line 14
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
            echo "

            ";
            // line 31
            echo "
        ";
        } else {
            // line 33
            echo "            <p>No hi han jugadors registrats</p>
            <a href=\"./registre\">Registre</a>
        ";
        }
        // line 36
        echo "
        ";
        // line 37
        if (array_key_exists("preguntes", $context)) {
            // line 38
            echo "            ";
            // line 57
            echo "        ";
        } else {
            // line 58
            echo "            <p>No hi han preguntes registrades</p>
            <a href=\"./crearPregunta\">Registre</a>
        ";
        }
        
        $__internal_c23b2046682b892c4a4a5967e1d296f2b505b709d56321b5cab59b62bf8280c4->leave($__internal_c23b2046682b892c4a4a5967e1d296f2b505b709d56321b5cab59b62bf8280c4_prof);

        
        $__internal_5854d2fc19ecf802bee338c9f0795e9af155c85a459b0c7e513c7cbd6ead0a12->leave($__internal_5854d2fc19ecf802bee338c9f0795e9af155c85a459b0c7e513c7cbd6ead0a12_prof);

    }

    public function getTemplateName()
    {
        return "portada/portada.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 58,  154 => 57,  152 => 38,  150 => 37,  147 => 36,  142 => 33,  138 => 31,  133 => 14,  129 => 13,  125 => 12,  122 => 11,  120 => 10,  117 => 9,  108 => 8,  90 => 6,  77 => 4,  68 => 3,  55 => 6,  52 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block head %}
    {% block stylesheets %}
        <link href=\"{{ asset('css/portada.css') }}\" type=\"text/css\" rel=\"stylesheet\" />
    {% endblock %}
<title>{% block title %}Trivial Pursuit{% endblock %} </title>
{% endblock %}
{% block body %}
    <h3> Portada Trivial </h3>
        {% if jugadors is defined %}
        <p>Aquest portal et permet jugar contestant una sèrie de preguntes i obtenint una puntuació</p>
            {{ form_start(form) }}
            {{ form_widget(form) }}
            {{ form_end(form) }}

            {#<h3> Jugadors Inscrits </h3>
            <table id=\"tblJugadors\">
                <tr>
                    <th>Nom</th>
                    <th>Puntuacio</th>
                    <th>Posició</th>
                </tr>
                {% for jugador in jugadors %}
                    <tr>
                        <td>{{ jugador.nom }}</td>
                        <td>{{ jugador.puntuacio }}</td>
                        <td>{{ jugador.posicio }}</td>
                    </tr>
                {% endfor %}
            </table>#}

        {% else %}
            <p>No hi han jugadors registrats</p>
            <a href=\"./registre\">Registre</a>
        {% endif %}

        {% if preguntes is defined %}
            {#<h3> Preguntes Disponibles </h3>
            <table id=\"tblPreguntes\">
                <tr>
                    <th>Enunciat</th>
                    <th>Resposta1</th>
                    <th>Resposta2</th>
                    <th>Resposta3</th>
                    <th>Resposta4</th>
                </tr>
                {% for pregunta in preguntes %}
                    <tr>
                        <td>{{ pregunta.enunciat }}</td>
                        <td>{{ pregunta.resposta1 }}</td>
                        <td>{{ pregunta.resposta2 }}</td>
                        <td>{{ pregunta.resposta3 }}</td>
                        <td>{{ pregunta.resposta4 }}</td>
                    </tr>
                {% endfor %}
            </table>#}
        {% else %}
            <p>No hi han preguntes registrades</p>
            <a href=\"./crearPregunta\">Registre</a>
        {% endif %}
{% endblock %}", "portada/portada.html.twig", "/home/ausias/Escriptori/projectes_symfony/JocTrivialIII/app/Resources/views/portada/portada.html.twig");
    }
}
