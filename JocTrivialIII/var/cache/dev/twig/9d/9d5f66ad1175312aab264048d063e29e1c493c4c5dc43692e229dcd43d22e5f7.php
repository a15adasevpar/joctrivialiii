<?php

/* base.html.twig */
class __TwigTemplate_d81bae7d5325a7f23ba92e849144467ab8ed2421c201cbf1d70c17582ad76de7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a16b29eae678e7e94993d5129f38f5f3cc4e2cb36e8ba60d1cabcc5973217b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a16b29eae678e7e94993d5129f38f5f3cc4e2cb36e8ba60d1cabcc5973217b6->enter($__internal_8a16b29eae678e7e94993d5129f38f5f3cc4e2cb36e8ba60d1cabcc5973217b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_359859f61e5e4bdc5c2c75f211c2090d255315b8378fb62da0abe7bfb063d17a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_359859f61e5e4bdc5c2c75f211c2090d255315b8378fb62da0abe7bfb063d17a->enter($__internal_359859f61e5e4bdc5c2c75f211c2090d255315b8378fb62da0abe7bfb063d17a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_8a16b29eae678e7e94993d5129f38f5f3cc4e2cb36e8ba60d1cabcc5973217b6->leave($__internal_8a16b29eae678e7e94993d5129f38f5f3cc4e2cb36e8ba60d1cabcc5973217b6_prof);

        
        $__internal_359859f61e5e4bdc5c2c75f211c2090d255315b8378fb62da0abe7bfb063d17a->leave($__internal_359859f61e5e4bdc5c2c75f211c2090d255315b8378fb62da0abe7bfb063d17a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_f16d5bcddea5093ab18b5872de1c6454d1044aeeb829108bb683262cf1b43e49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f16d5bcddea5093ab18b5872de1c6454d1044aeeb829108bb683262cf1b43e49->enter($__internal_f16d5bcddea5093ab18b5872de1c6454d1044aeeb829108bb683262cf1b43e49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_b62f422966c39b07c20a0e590f7174b39f2a60f4294ca7875bc9d26fe69540e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b62f422966c39b07c20a0e590f7174b39f2a60f4294ca7875bc9d26fe69540e0->enter($__internal_b62f422966c39b07c20a0e590f7174b39f2a60f4294ca7875bc9d26fe69540e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_b62f422966c39b07c20a0e590f7174b39f2a60f4294ca7875bc9d26fe69540e0->leave($__internal_b62f422966c39b07c20a0e590f7174b39f2a60f4294ca7875bc9d26fe69540e0_prof);

        
        $__internal_f16d5bcddea5093ab18b5872de1c6454d1044aeeb829108bb683262cf1b43e49->leave($__internal_f16d5bcddea5093ab18b5872de1c6454d1044aeeb829108bb683262cf1b43e49_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ab53201889b8365250bbe5136e27665839152df0fc67e868aa4b31961e8186e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab53201889b8365250bbe5136e27665839152df0fc67e868aa4b31961e8186e3->enter($__internal_ab53201889b8365250bbe5136e27665839152df0fc67e868aa4b31961e8186e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_1c0166ffa022e8c686ccc3895c1712f5afe10254cff1c62cc49cf62f9be28ec8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c0166ffa022e8c686ccc3895c1712f5afe10254cff1c62cc49cf62f9be28ec8->enter($__internal_1c0166ffa022e8c686ccc3895c1712f5afe10254cff1c62cc49cf62f9be28ec8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_1c0166ffa022e8c686ccc3895c1712f5afe10254cff1c62cc49cf62f9be28ec8->leave($__internal_1c0166ffa022e8c686ccc3895c1712f5afe10254cff1c62cc49cf62f9be28ec8_prof);

        
        $__internal_ab53201889b8365250bbe5136e27665839152df0fc67e868aa4b31961e8186e3->leave($__internal_ab53201889b8365250bbe5136e27665839152df0fc67e868aa4b31961e8186e3_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_2a0fca2801c6447b6d080107321640c97810f0d7c074350fdc0249607dadff32 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a0fca2801c6447b6d080107321640c97810f0d7c074350fdc0249607dadff32->enter($__internal_2a0fca2801c6447b6d080107321640c97810f0d7c074350fdc0249607dadff32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_850d6b2a35d5c5882b112758c98dcf162c5e9ef5354b3982f146e03e7d148729 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_850d6b2a35d5c5882b112758c98dcf162c5e9ef5354b3982f146e03e7d148729->enter($__internal_850d6b2a35d5c5882b112758c98dcf162c5e9ef5354b3982f146e03e7d148729_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_850d6b2a35d5c5882b112758c98dcf162c5e9ef5354b3982f146e03e7d148729->leave($__internal_850d6b2a35d5c5882b112758c98dcf162c5e9ef5354b3982f146e03e7d148729_prof);

        
        $__internal_2a0fca2801c6447b6d080107321640c97810f0d7c074350fdc0249607dadff32->leave($__internal_2a0fca2801c6447b6d080107321640c97810f0d7c074350fdc0249607dadff32_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_08e47d12c53b0ac02d3d3d1705f67f8d4081a81768842db426eae277b24d13d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08e47d12c53b0ac02d3d3d1705f67f8d4081a81768842db426eae277b24d13d7->enter($__internal_08e47d12c53b0ac02d3d3d1705f67f8d4081a81768842db426eae277b24d13d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_2b124f604592327e8add28ec3f94ff4f987ae299d17370bfdf5506e97290d1e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2b124f604592327e8add28ec3f94ff4f987ae299d17370bfdf5506e97290d1e6->enter($__internal_2b124f604592327e8add28ec3f94ff4f987ae299d17370bfdf5506e97290d1e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_2b124f604592327e8add28ec3f94ff4f987ae299d17370bfdf5506e97290d1e6->leave($__internal_2b124f604592327e8add28ec3f94ff4f987ae299d17370bfdf5506e97290d1e6_prof);

        
        $__internal_08e47d12c53b0ac02d3d3d1705f67f8d4081a81768842db426eae277b24d13d7->leave($__internal_08e47d12c53b0ac02d3d3d1705f67f8d4081a81768842db426eae277b24d13d7_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 11,  100 => 10,  83 => 6,  65 => 5,  53 => 12,  50 => 11,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/ausias/Escriptori/projectes_symfony/joctrivialiii/JocTrivialIII/app/Resources/views/base.html.twig");
    }
}
